module.exports = {
  env: {
    es6: true,
    node: true
  },
  extends: ['xo-space/esnext', 'prettier'],
  plugins: ['prettier'],
  rules: {
    'arrow-body-style': ['error', 'as-needed'],
    'prettier/prettier': [
      'error',
      {
        singleQuote: true,
        bracketSpacing: false
      }
    ]
  }
};
