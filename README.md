# FPL Hub server

> Here exists the FPL Hub server

## Setup

``` bash
# Install dependencies
npm install

# Start the app
npm start
```

## Deployment

``` bash
git push heroku master
```
