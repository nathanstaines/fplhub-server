const path = require('path');

module.exports = {
  protocol: undefined,
  host: undefined,
  port: undefined,
  public: path.resolve(__dirname, '../public/'),
  api: {
    keys: undefined
  },
  auth: {
    secret: undefined,
    service: 'api/users',
    authStrategies: ['jwt', 'local', 'apiKey'],
    entity: 'user',
    entityId: '_id',
    jwtOptions: {
      header: {
        typ: 'access'
      },
      audience: 'https://fplhub.com',
      issuer: 'feathers',
      algorithm: 'HS256',
      expiresIn: '1d'
    },
    local: {
      usernameField: 'username',
      passwordField: 'password'
    }
  },
  database: {
    uri: undefined,
    name: undefined,
    paginate: {
      default: 25,
      max: 50
    }
  },
  cors: {
    origin: undefined
  }
};
