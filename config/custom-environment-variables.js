module.exports = {
  protocol: 'PROTOCOL',
  host: 'HOST',
  port: 'PORT',
  api: {
    keys: 'API_KEYS'
  },
  auth: {
    secret: 'AUTH_SECRET'
  },
  database: {
    uri: 'MONGODB_URI',
    name: 'MONGODB_NAME'
  },
  cors: {
    origin: 'CORS_ORIGIN'
  }
};
