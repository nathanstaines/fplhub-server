const app = require('../app');
const cron = require('cron');
const logger = require('../utils/logger');

const jobs = {
  // Run whenever the server starts, limited to once a day.
  bootstrapAll: new cron.CronJob({
    cronTime: '0 * * * * *',
    onTick() {
      logger.info('running cron job: bootstrapAll');

      app.service('api/bootstrap-players').find();
      app.service('api/bootstrap-fixtures').find();

      checkFixtures();

      app
        .service('api/jobs')
        .update('bootstrapAll', {
          isEnabled: false
        })
        .then(() => this.stop())
        .catch(err => logger.error(err));
    },
    timeZone: 'Europe/London'
  }),
  bootstrapFixtures1: new cron.CronJob({
    cronTime: '0 * 12-18 * * *',
    onTick: () => {
      logger.info('running cron job: bootstrapFixtures1');

      bootstrapFixtures();
    },
    timeZone: 'Europe/London'
  }),
  bootstrapFixtures2: new cron.CronJob({
    cronTime: '0 0-29/1 19 * * *',
    onTick: () => {
      logger.info('running cron job: bootstrapFixtures2');

      bootstrapFixtures();
    },
    timeZone: 'Europe/London'
  }),
  bootstrapFixtures3: new cron.CronJob({
    cronTime: '0 30-59/1 19 * * *',
    onTick: () => {
      logger.info('running cron job: bootstrapFixtures3');

      bootstrapFixtures();
    },
    timeZone: 'Europe/London'
  }),
  bootstrapFixtures4: new cron.CronJob({
    cronTime: '0 * 20-21 * * *',
    onTick: () => {
      logger.info('running cron job: bootstrapFixtures4');

      bootstrapFixtures();
    },
    timeZone: 'Europe/London'
  }),
  bootstrapPlayers: new cron.CronJob({
    cronTime: '0 0 * * * *',
    onTick: () => {
      logger.info('running cron job: bootstrapPlayers');

      app.service('api/bootstrap-players').find();
    },
    timeZone: 'Europe/London'
  }),
  checkFixtures: new cron.CronJob({
    cronTime: '0 0 10 * * *',
    onTick: () => {
      logger.info('running cron job: checkFixtures');

      checkFixtures();
    },
    timeZone: 'Europe/London'
  })
};

function bootstrapFixtures() {
  app.service('api/bootstrap-fixtures').find({
    query: {
      isCurrentEvent: true
    }
  });
}

function checkFixtures() {
  const now = new Date();
  const year = now.getFullYear();
  const month = now.getMonth();
  const day = now.getDate();

  app
    .service('api/fixtures')
    .find({
      query: {
        kickoffTime: {
          $gte: new Date(year, month, day, 10),
          $lt: new Date(year, month, day, 19, 30)
        }
      }
    })
    .then(res => {
      if (res.data.length) {
        updateJob('start', 'bootstrapFixtures1');
        updateJob('start', 'bootstrapFixtures2');
      } else {
        updateJob('stop', 'bootstrapFixtures1');
        updateJob('stop', 'bootstrapFixtures2');
      }
    })
    .catch(err => logger.error(err));

  app
    .service('api/fixtures')
    .find({
      query: {
        kickoffTime: {
          $gte: new Date(year, month, day, 19, 30),
          $lt: new Date(year, month, day, 24)
        }
      }
    })
    .then(res => {
      if (res.data.length) {
        updateJob('start', 'bootstrapFixtures3');
        updateJob('start', 'bootstrapFixtures4');
      } else {
        updateJob('stop', 'bootstrapFixtures3');
        updateJob('stop', 'bootstrapFixtures4');
      }
    })
    .catch(err => logger.error(err));
}

function updateJob(action, job) {
  const isEnabled = action === 'start';
  const isRunning = Boolean(jobs[job].running);

  app
    .service('api/jobs')
    .get(job)
    .then(res => {
      if (res.isEnabled === isEnabled && isRunning === isEnabled) {
        return;
      }

      toggleJob(action, job, isEnabled);
    })
    .catch(err => {
      let error = err;

      if (typeof err === 'object') {
        error = JSON.stringify(err);
      }

      if (error.includes('No record found')) {
        toggleJob(action, job, isEnabled);
      } else {
        logger.error(error);
      }
    });
}

function toggleJob(action, job, isEnabled) {
  return app
    .service('api/jobs')
    .update(job, {isEnabled}, {mongodb: {upsert: true}})
    .then(() => {
      if (isEnabled) {
        jobs[job].start();
      } else {
        jobs[job].stop();
      }

      logger.info(`${action} cron job: ${job}`);
    })
    .catch(err => logger.error(err));
}

module.exports = function(title) {
  return jobs[title];
};
