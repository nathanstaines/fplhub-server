// Round to 1 decimal place without converting to a string.
module.exports = function(num) {
  return Math.round(num * 1e1) / 1e1;
};
