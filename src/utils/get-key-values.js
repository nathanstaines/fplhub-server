function isArray(val) {
  return Array.isArray(val);
}

function isObject(val) {
  return val !== null && typeof val === 'object';
}

function getKeyValues(obj, key) {
  const arr = isArray(obj) ? obj : [obj];

  return arr.reduce((a, b) => {
    Object.keys(b).forEach(val => {
      if (isObject(b[val])) {
        a = a.concat(getKeyValues(b[val], key));
      }

      if (val === key) {
        a = a.concat(b[val]);
      }
    });

    return a;
  }, []);
}

module.exports = function(obj, key) {
  return getKeyValues(obj, key);
};
