function isBoolean(val) {
  return val === 'false' || val === 'true';
}

function isArray(val) {
  return Array.isArray(val);
}

function isObject(val) {
  return val !== null && typeof val === 'object';
}

function isNumber(val) {
  return !isNaN(parseFloat(val)) && isFinite(val);
}

function parseBoolean(val) {
  return val === 'true';
}

function parseArray(arr) {
  return arr.map(item => parseValue(item));
}

function parseObject(obj) {
  const ret = {};

  Object.keys(obj).forEach(key => {
    ret[key] = parseValue(obj[key]);
  });

  return ret;
}

function parseNumber(num) {
  return Number(num);
}

function parseValue(val) {
  if (typeof val === 'undefined' || String(val) === 'null' || val === '') {
    return null;
  }

  if (isBoolean(val)) {
    return parseBoolean(val);
  }

  if (isArray(val)) {
    return parseArray(val);
  }

  if (isObject(val)) {
    return parseObject(val);
  }

  if (isNumber(val)) {
    return parseNumber(val);
  }

  return val;
}

module.exports = function(val) {
  return parseValue(val);
};
