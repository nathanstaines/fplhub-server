const logger = require('../utils/logger');
const parseValue = require('../utils/parse-value');

module.exports = function() {
  return context => {
    Promise.all(
      context.result.element_types.map(position => {
        const props = {
          singularName: parseValue(position.singular_name),
          singularNameShort: parseValue(position.singular_name_short),
          pluralName: parseValue(position.plural_name),
          pluralNameShort: parseValue(position.plural_name_short)
        };

        return context.app.service('api/positions').update(position.id, props, {
          mongodb: {
            upsert: true
          }
        });
      })
    ).then(positions => {
      logger.info(`${positions.length} positions updated`);

      return context;
    });
  };
};
