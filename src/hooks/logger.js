const logger = require('../utils/logger');
const util = require('util');

module.exports = function() {
  return context => {
    const message = `${context.type}: ${context.path} - method: ${context.method}`;

    logger.info(message);

    if (typeof context.toJSON === 'function' && logger.level === 'debug') {
      logger.debug(`hook context: ${util.inspect(context, {colors: false})}`);
    }

    if (context.error) {
      logger.error(context.error.stack);
    }
  };
};
