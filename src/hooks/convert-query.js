const parseValue = require('../utils/parse-value');

module.exports = function() {
  return context => {
    context.params.query = parseValue(context.params.query) || {};

    return context;
  };
};
