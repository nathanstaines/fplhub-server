module.exports = function() {
  return context => {
    const {params} = context;

    // #TODO: Consider skipping this if `params.authentication` already exists?
    if (params.provider && params.headers && params.headers['x-api-key']) {
      context.params.authentication = {
        strategy: 'apiKey',
        apiKey: params.headers['x-api-key']
      };
    }

    return context;
  };
};
