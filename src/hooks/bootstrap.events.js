const logger = require('../utils/logger');
const parseValue = require('../utils/parse-value');

module.exports = function() {
  return context => {
    Promise.all(
      context.result.events.map(event => {
        const props = {
          name: parseValue(event.name),
          deadlineTime: new Date(parseValue(event.deadline_time)),
          hasFinished: parseValue(event.finished),
          isPrevious: parseValue(event.is_previous),
          isCurrent: parseValue(event.is_current),
          isNext: parseValue(event.is_next)
        };

        return context.app.service('api/events').update(event.id, props, {
          mongodb: {
            upsert: true
          }
        });
      })
    )
      .then(events => {
        logger.info(`${events.length} events updated`);

        return context;
      })
      .catch(err => logger.error(err));
  };
};
