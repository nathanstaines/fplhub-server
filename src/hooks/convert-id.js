module.exports = function() {
  return context => {
    context.id = parseInt(context.id, 10);

    return context;
  };
};
