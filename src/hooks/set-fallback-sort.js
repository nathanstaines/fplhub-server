module.exports = function() {
  return context => {
    context.params.query = context.params.query || {};

    context.params.query.$sort = context.params.query.$sort || {
      normalizedWebName: 1
    };

    return context;
  };
};
