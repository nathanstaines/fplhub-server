const logger = require('../utils/logger');
const parseValue = require('../utils/parse-value');

module.exports = function() {
  return context => {
    Promise.all(
      context.result.teams.map(team => {
        const props = {
          code: parseValue(team.code),
          name: parseValue(team.name),
          shortName: parseValue(team.short_name)
        };

        return context.app.service('api/teams').update(team.id, props, {
          mongodb: {
            upsert: true
          }
        });
      })
    ).then(teams => {
      logger.info(`${teams.length} teams updated`);

      return context;
    });
  };
};
