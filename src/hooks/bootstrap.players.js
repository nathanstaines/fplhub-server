const Bottleneck = require('bottleneck');
const logger = require('../utils/logger');
const normalizeName = require('../utils/normalize-name');
const parseValue = require('../utils/parse-value');
const roundNumber = require('../utils/round-number');

const limiter = new Bottleneck({
  maxConcurrent: 1,
  minTime: 100
});

const statsMap = {
  price: {
    ref: 'now_cost',
    isGlobal: true
  },
  value: {
    ref: 'value_form',
    isGlobal: true
  },
  form: {
    ref: 'form',
    isGlobal: true
  },
  points: {
    ref: 'total_points'
  },
  xPoints: {
    ref: 'ep_next',
    isGlobal: true
  },
  minutes: {
    ref: 'minutes'
  },
  goalsScored: {
    ref: 'goals_scored'
  },
  assists: {
    ref: 'assists'
  },
  cleanSheets: {
    ref: 'clean_sheets'
  },
  goalsConceded: {
    ref: 'goals_conceded'
  },
  ownGoals: {
    ref: 'own_goals'
  },
  yellowCards: {
    ref: 'yellow_cards'
  },
  redCards: {
    ref: 'red_cards'
  },
  saves: {
    ref: 'saves'
  },
  bonus: {
    ref: 'bonus'
  },
  bonusPointsSystem: {
    ref: 'bps'
  },
  influence: {
    ref: 'influence',
    isGlobal: true
  },
  creativity: {
    ref: 'creativity',
    isGlobal: true
  },
  threat: {
    ref: 'threat',
    isGlobal: true
  },
  ICTIndex: {
    ref: 'ict_index',
    isGlobal: true
  },
  selectedPercent: {
    ref: 'selected_by_percent',
    isGlobal: true
  }
};

const teams = {};

module.exports = function() {
  return context => {
    const isPreseason = context.result.events[0].is_next;

    Promise.all(
      context.result.elements.map(player => {
        const playerProps = {
          firstName: parseValue(player.first_name),
          secondName: parseValue(player.second_name),
          webName: parseValue(player.web_name),
          normalizedFullName: normalizeName(
            `${parseValue(player.first_name)} ${parseValue(player.second_name)}`
          ),
          normalizedWebName: normalizeName(parseValue(player.web_name)),
          position: parseValue(player.element_type),
          team: parseValue(player.team),
          teamCode: parseValue(player.team_code),
          news: parseValue(player.news),
          stats: {},
          gameHistory: [],
          seasonHistory: [],
          fixtureDifficulty: []
        };

        Object.keys(statsMap).forEach(stat => {
          if (isPreseason || statsMap[stat].isGlobal) {
            playerProps.stats[stat] = {
              active: parseValue(player[statsMap[stat].ref]),
              summary: parseValue(player[statsMap[stat].ref]),
              total: parseValue(player[statsMap[stat].ref])
            };

            if (statsMap[stat].isGlobal) {
              playerProps.stats[stat].isGlobal = true;
            }
          } else {
            playerProps.stats[stat] = {
              active: 0,
              summary: 0,
              total: 0
            };
          }
        });

        playerProps.stats.gamesPlayed = {
          active: 0,
          summary: 0,
          total: 0
        };

        return limiter
          .schedule(() =>
            context.app.service('api/bootstrap-players').get(player.id)
          )
          .then(summary => {
            const totalGames = summary.history.length;

            summary.history.forEach((game, i) => {
              const isActiveTimeframe = i === totalGames - 1;
              const isSummaryTimeframe = i >= totalGames - 6;
              const gameProps = {};

              gameProps.round = parseValue(game.round);

              Object.keys(statsMap).forEach(stat => {
                if (!statsMap[stat].isGlobal) {
                  gameProps[stat] = parseValue(game[statsMap[stat].ref]);

                  if (isActiveTimeframe) {
                    playerProps.stats[stat].active += parseValue(
                      game[statsMap[stat].ref]
                    );
                  }

                  if (isSummaryTimeframe) {
                    playerProps.stats[stat].summary += parseValue(
                      game[statsMap[stat].ref]
                    );
                  }

                  playerProps.stats[stat].total += parseValue(
                    game[statsMap[stat].ref]
                  );
                }
              });

              gameProps.price = parseValue(game.value);

              if (game.minutes) {
                if (isActiveTimeframe) {
                  playerProps.stats.gamesPlayed.active++;
                }

                if (isSummaryTimeframe) {
                  playerProps.stats.gamesPlayed.summary++;
                }

                playerProps.stats.gamesPlayed.total++;
              }

              playerProps.gameHistory.push(gameProps);
            });

            summary.history_past.forEach(season => {
              const seasonProps = {};

              seasonProps.name = parseValue(season.season_name);

              Object.keys(statsMap).forEach(stat => {
                if (!statsMap[stat].isGlobal) {
                  seasonProps[stat] = parseValue(season[statsMap[stat].ref]);
                }
              });

              playerProps.seasonHistory.push(seasonProps);
            });

            if (isPreseason) {
              playerProps.stats.pointsPerGame = {
                active: parseValue(player.points_per_game),
                summary: parseValue(player.points_per_game),
                total: parseValue(player.points_per_game)
              };
            } else {
              playerProps.stats.pointsPerGame = {
                active: playerProps.stats.points.active,
                summary: 0,
                total: 0
              };

              if (playerProps.stats.points.summary) {
                playerProps.stats.pointsPerGame.summary = roundNumber(
                  playerProps.stats.points.summary /
                    playerProps.stats.gamesPlayed.summary
                );
              }

              if (playerProps.stats.points.total) {
                playerProps.stats.pointsPerGame.total = roundNumber(
                  playerProps.stats.points.total /
                    playerProps.stats.gamesPlayed.total
                );
              }
            }

            if (!{}.hasOwnProperty.call(teams, player.team)) {
              teams[player.team] = {
                fixtureDifficulty: summary.fixtures
                  .slice(0, 6)
                  .map(fixture => ({
                    id: fixture.code,
                    isHome: fixture.is_home,
                    difficulty: fixture.difficulty,
                    opponentTeamId: fixture.is_home
                      ? fixture.team_a
                      : fixture.team_h
                  }))
              };
            }

            playerProps.fixtureDifficulty =
              teams[player.team].fixtureDifficulty;

            return context.app
              .service('api/players')
              .update(player.id, playerProps, {
                mongodb: {
                  upsert: true
                }
              });
          })
          .catch(err => logger.error(err));
      })
    ).then(players => {
      logger.info(`${players.length} players updated`);

      return context;
    });
  };
};
