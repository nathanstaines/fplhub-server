const format = require('date-fns/format');
const getKeyValues = require('../utils/get-key-values');
const logger = require('../utils/logger');
const parseValue = require('../utils/parse-value');

const statMap = {
  goalsScored: {
    ref: 'goals_scored',
    title: 'Goals'
  },
  assists: {
    ref: 'assists',
    title: 'Assists'
  },
  ownGoals: {
    ref: 'own_goals',
    title: 'Own goals'
  },
  penaltiesSaved: {
    ref: 'penalties_saved',
    title: 'Penalties saved'
  },
  penaltiesMissed: {
    ref: 'penalties_missed',
    title: 'Penalties missed'
  },
  yellowCards: {
    ref: 'yellow_cards',
    title: 'Yellow cards'
  },
  redCards: {
    ref: 'red_cards',
    title: 'Red cards'
  },
  saves: {
    ref: 'saves',
    title: 'Saves'
  },
  bonus: {
    ref: 'bonus',
    title: 'Bonus'
  },
  bonusPointsSystem: {
    ref: 'bps',
    title: 'Bonus points system',
    limit: 5
  }
};

const events = {};
const players = {};

let currentEvent;
let fixtures;

module.exports = function() {
  return context => {
    const {query = {}} = context.params;

    context.app
      .service('api/events')
      .find()
      .then(res => {
        res.data.forEach(event => {
          events[event.id] = event;

          if (event.isCurrent) {
            currentEvent = event.id;
          }
        });
      })
      .then(() => {
        if (query.isCurrentEvent) {
          fixtures = context.result.filter(
            fixture => fixture.event === currentEvent && !fixture.finished
          );
        } else {
          fixtures = context.result;
        }

        Promise.all(
          fixtures
            .filter(fixture => fixture.stats)
            .map(fixture => getKeyValues(fixture.stats, 'element'))
        )
          .then(playerIds => {
            const promises = [];

            if (playerIds.length) {
              Array.from(new Set([].concat(...playerIds))).forEach(id => {
                promises.push(
                  context.app
                    .service('api/players')
                    .get(id)
                    .then(player => {
                      players[id] = {
                        webName: player.webName
                      };
                    })
                    .catch(err => logger.error(err))
                );
              });
            }

            return Promise.all(promises);
          })
          .then(() => {
            Promise.all(
              fixtures
                .filter(fixture => fixture.event)
                .map(fixture => {
                  const hScore = parseValue(fixture.team_h_score);
                  const aScore = parseValue(fixture.team_a_score);

                  const kickoffTime = new Date(
                    parseValue(fixture.kickoff_time)
                  );

                  const props = {
                    eventDay: format(kickoffTime, 'yyyyMMdd'),
                    stats: {},
                    kickoffTime,
                    homeTeamScore: hScore === null ? 0 : hScore,
                    awayTeamScore: aScore === null ? 0 : aScore,
                    hasStarted: parseValue(fixture.started),
                    hasFinished: parseValue(fixture.finished_provisional),
                    isCurrentEvent: parseValue(events[fixture.event].isCurrent),
                    minutes: parseValue(fixture.minutes),
                    event: parseValue(fixture.event),
                    homeTeam: parseValue(fixture.team_h),
                    awayTeam: parseValue(fixture.team_a)
                  };

                  function handleItem(arr, item) {
                    arr.push({
                      player: players[item.element],
                      value: item.value
                    });
                  }

                  function handleMapStat(stat, mapStat) {
                    if (stat.identifier === statMap[mapStat].ref) {
                      const {title, limit} = statMap[mapStat];
                      const home = [];
                      const away = [];

                      stat.h.forEach(item => handleItem(home, item));
                      stat.a.forEach(item => handleItem(away, item));

                      if (home.length || away.length) {
                        props.stats[mapStat] = {
                          title,
                          home,
                          away
                        };

                        if (limit) {
                          props.stats[mapStat].limit = limit;
                        }
                      }
                    }
                  }

                  function handleStat(stat) {
                    Object.keys(statMap).forEach(mapStat =>
                      handleMapStat(stat, mapStat)
                    );
                  }

                  if (fixture.stats) {
                    fixture.stats.forEach(stat => handleStat(stat));
                  }

                  return context.app
                    .service('api/fixtures')
                    .update(fixture.id, props, {
                      mongodb: {
                        upsert: true
                      }
                    });
                })
            )
              .then(fixtures => {
                context.app.service('api/fixtures').emit('bootstrapped');
                logger.info(`${fixtures.length} fixtures updated`);

                return context;
              })
              .catch(err => logger.error(err));
          })
          .catch(err => logger.error(err));
      })
      .catch(err => logger.error(err));
  };
};
