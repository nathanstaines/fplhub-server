const createService = require('feathers-mongodb');
const hooks = require('./teams.hooks');

module.exports = function(app) {
  const mongoClient = app.get('mongoClient');

  const options = {
    id: 'id',
    paginate: {
      ...app.get('database').paginate,
      default: 20
    }
  };

  app.use('/api/teams', createService(options));

  const service = app.service('api/teams');

  mongoClient.then(db => {
    service.Model = db.collection('teams');
  });

  service.hooks(hooks);
};
