const bootstrapFixtures = require('./bootstrap-fixtures/bootstrap-fixtures.service');
const bootstrapPlayers = require('./bootstrap-players/bootstrap-players.service');
const events = require('./events/events.service');
const fixtures = require('./fixtures/fixtures.service');
const jobs = require('./jobs/jobs.service');
const players = require('./players/players.service');
const positions = require('./positions/positions.service');
const teams = require('./teams/teams.service');
const users = require('./users/users.service');

module.exports = function(app) {
  app.configure(bootstrapFixtures);
  app.configure(bootstrapPlayers);
  app.configure(events);
  app.configure(fixtures);
  app.configure(jobs);
  app.configure(players);
  app.configure(positions);
  app.configure(teams);
  app.configure(users);
};
