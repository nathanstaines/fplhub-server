const axios = require('axios');
const logger = require('../../utils/logger');

const instance = axios.create({
  baseURL: 'https://fantasy.premierleague.com/api',
  headers: {
    'User-Agent': 'Undercover'
  }
});

module.exports = function() {
  return {
    find() {
      return instance
        .get('/fixtures/')
        .then(res => res.data)
        .catch(err => logger.error(err));
    }
  };
};
