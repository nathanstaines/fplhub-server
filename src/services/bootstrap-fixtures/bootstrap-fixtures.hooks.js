const {authenticate} = require('@feathersjs/authentication').hooks;
const bootstrapFixtures = require('../../hooks/bootstrap.fixtures');
const logger = require('../../hooks/logger');

module.exports = {
  before: {
    find: authenticate('jwt')
  },
  after: {
    find: [bootstrapFixtures(), logger()]
  }
};
