const adapter = require('./bootstrap-fixtures.adapter');
const hooks = require('./bootstrap-fixtures.hooks');

module.exports = function(app) {
  app.use('/api/bootstrap-fixtures', adapter());
  app.service('api/bootstrap-fixtures').hooks(hooks);
};
