const adapter = require('./bootstrap-players.adapter');
const hooks = require('./bootstrap-players.hooks');

module.exports = function(app) {
  app.use('/api/bootstrap-players', adapter());
  app.service('api/bootstrap-players').hooks(hooks);
};
