const {authenticate} = require('@feathersjs/authentication').hooks;
const bootstrapEvents = require('../../hooks/bootstrap.events');
const bootstrapPlayers = require('../../hooks/bootstrap.players');
const bootstrapPositions = require('../../hooks/bootstrap.positions');
const bootstrapTeams = require('../../hooks/bootstrap.teams');
const logger = require('../../hooks/logger');

module.exports = {
  before: {
    find: authenticate('jwt')
  },
  after: {
    find: [
      bootstrapEvents(),
      bootstrapPlayers(),
      bootstrapPositions(),
      bootstrapTeams(),
      logger()
    ]
  }
};
