const axios = require('axios');

const instance = axios.create({
  baseURL: 'https://fantasy.premierleague.com/api',
  headers: {
    'User-Agent': 'Undercover'
  }
});

module.exports = function() {
  return {
    find() {
      return instance.get('/bootstrap-static/').then(res => res.data);
    },
    get(id) {
      return instance.get(`/element-summary/${id}/`).then(res => res.data);
    }
  };
};
