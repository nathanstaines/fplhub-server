const createService = require('feathers-mongodb');
const hooks = require('./positions.hooks');

module.exports = function(app) {
  const mongoClient = app.get('mongoClient');

  const options = {
    id: 'id',
    paginate: {
      ...app.get('database').paginate,
      default: 4
    }
  };

  app.use('/api/positions', createService(options));

  const service = app.service('api/positions');

  mongoClient.then(db => {
    service.Model = db.collection('positions');
  });

  service.hooks(hooks);
};
