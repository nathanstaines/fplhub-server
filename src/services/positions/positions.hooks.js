const {authenticate} = require('@feathersjs/authentication').hooks;
const authenticateApiKey = require('../../hooks/authenticate-api-key');

module.exports = {
  before: {
    all: [authenticateApiKey(), authenticate('apiKey')]
  }
};
