const {authenticate} = require('@feathersjs/authentication').hooks;
const {populate} = require('feathers-hooks-common');
const authenticateApiKey = require('../../hooks/authenticate-api-key');
const logger = require('../../hooks/logger');
const parseValue = require('../../utils/parse-value');

const schema = {
  include: [
    {
      service: 'api/fixtures',
      nameAs: 'fixtures',
      parentField: 'id',
      childField: 'event',
      query: {
        $sort: {
          kickoffTime: 1
        }
      },
      include: [
        {
          service: 'api/teams',
          nameAs: 'homeTeamDetails',
          parentField: 'homeTeam',
          childField: 'id'
        },
        {
          service: 'api/teams',
          nameAs: 'awayTeamDetails',
          parentField: 'awayTeam',
          childField: 'id'
        }
      ]
    }
  ]
};

function parseQuery() {
  return context => {
    const {query = {}} = context.params;

    context.params.query = parseValue(query);
  };
}

module.exports = {
  before: {
    all: [authenticateApiKey(), authenticate('apiKey')],
    find: parseQuery()
  },
  after: {
    all: populate({schema}),
    find: logger(),
    get: logger()
  }
};
