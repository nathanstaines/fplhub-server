const createService = require('feathers-mongodb');
const hooks = require('./events.hooks');

module.exports = function(app) {
  const mongoClient = app.get('mongoClient');

  const options = {
    id: 'id',
    paginate: {
      ...app.get('database').paginate,
      default: 38
    }
  };

  app.use('/api/events', createService(options));

  const service = app.service('api/events');

  mongoClient.then(db => {
    service.Model = db.collection('events');
  });

  service.hooks(hooks);
};
