const createService = require('feathers-mongodb');
const hooks = require('./jobs.hooks');

module.exports = function(app) {
  const mongoClient = app.get('mongoClient');

  const options = {
    id: 'title',
    paginate: app.get('database').paginate
  };

  app.use('/api/jobs', createService(options));

  const service = app.service('api/jobs');

  mongoClient.then(db => {
    service.Model = db.collection('jobs');
  });

  service.hooks(hooks);
};
