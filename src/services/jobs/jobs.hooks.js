const logger = require('../../hooks/logger');
const {authenticate} = require('@feathersjs/authentication').hooks;
const {setNow} = require('feathers-hooks-common');

module.exports = {
  before: {
    all: authenticate('jwt'),
    create: setNow('createdAt', 'updatedAt'),
    update: setNow('updatedAt')
  },
  after: {
    all: logger()
  }
};
