const {authenticate} = require('@feathersjs/authentication').hooks;
const {populate} = require('feathers-hooks-common');
const {setNow} = require('feathers-hooks-common');
const authenticateApiKey = require('../../hooks/authenticate-api-key');
const convertId = require('../../hooks/convert-id');
const convertQuery = require('../../hooks/convert-query');
const logger = require('../../hooks/logger');
const search = require('feathers-mongodb-fuzzy-search');
const setFallbackSort = require('../../hooks/set-fallback-sort');

const schema = {
  include: [
    {
      service: 'api/positions',
      nameAs: 'positionDetails',
      parentField: 'position',
      childField: 'id'
    },
    {
      service: 'api/teams',
      nameAs: 'teamDetails',
      parentField: 'teamCode',
      childField: 'code'
    }
  ]
};

module.exports = {
  before: {
    all: [authenticateApiKey(), authenticate('apiKey')],
    find: [
      setFallbackSort(),
      convertQuery(),
      search({
        fields: ['normalizedFullName']
      })
    ],
    get: convertId(),
    update: setNow('updatedAt')
  },
  after: {
    all: populate({schema}),
    find: logger(),
    get: logger()
  }
};
