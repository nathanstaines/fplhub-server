const createService = require('feathers-mongodb');
const hooks = require('./players.hooks');

module.exports = function(app) {
  const mongoClient = app.get('mongoClient');

  const options = {
    id: 'id',
    paginate: app.get('database').paginate,
    whitelist: ['$regex']
  };

  app.use('/api/players', createService(options));

  const service = app.service('api/players');

  mongoClient.then(db => {
    service.Model = db.collection('players');
  });

  service.hooks(hooks);
};
