const createService = require('feathers-mongodb');
const hooks = require('./fixtures.hooks');

module.exports = function(app) {
  const mongoClient = app.get('mongoClient');

  const options = {
    id: 'id',
    paginate: app.get('database').paginate,
    events: ['bootstrapped']
  };

  app.use('/api/fixtures', createService(options));

  const service = app.service('api/fixtures');

  mongoClient.then(db => {
    service.Model = db.collection('fixtures');
  });

  service.hooks(hooks);
};
