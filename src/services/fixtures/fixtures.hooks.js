const {authenticate} = require('@feathersjs/authentication').hooks;
const {populate} = require('feathers-hooks-common');
const {setNow} = require('feathers-hooks-common');
const authenticateApiKey = require('../../hooks/authenticate-api-key');
const logger = require('../../hooks/logger');

const schema = {
  include: [
    {
      service: 'api/teams',
      nameAs: 'homeTeamDetails',
      parentField: 'homeTeam',
      childField: 'id'
    },
    {
      service: 'api/teams',
      nameAs: 'awayTeamDetails',
      parentField: 'awayTeam',
      childField: 'id'
    }
  ]
};

module.exports = {
  before: {
    all: [authenticateApiKey(), authenticate('apiKey')],
    update: setNow('updatedAt')
  },
  after: {
    all: populate({schema}),
    find: logger(),
    get: logger()
  }
};
