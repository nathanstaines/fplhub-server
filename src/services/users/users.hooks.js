const {authenticate} = require('@feathersjs/authentication').hooks;

const {
  hashPassword,
  protect
} = require('@feathersjs/authentication-local').hooks;

const logger = require('../../hooks/logger');
const {setField} = require('feathers-authentication-hooks');
const {setNow} = require('feathers-hooks-common');

const restrictToOwner = setField({
  from: 'params.user._id',
  as: 'params.query._id'
});

module.exports = {
  before: {
    find: authenticate('jwt'),
    get: [authenticate('jwt'), restrictToOwner],
    create: [hashPassword('password'), setNow('createdAt', 'updatedAt')],
    update: [
      authenticate('jwt'),
      restrictToOwner,
      hashPassword('password'),
      setNow('updatedAt')
    ],
    remove: [authenticate('jwt'), restrictToOwner]
  },
  after: {
    all: [logger(), protect('password')]
  }
};
