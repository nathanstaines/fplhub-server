const createService = require('feathers-mongodb');
const hooks = require('./users.hooks');

module.exports = function(app) {
  const mongoClient = app.get('mongoClient');

  const options = {
    paginate: app.get('database').paginate
  };

  app.use('/api/users', createService(options));

  const service = app.service('api/users');

  mongoClient.then(db => {
    service.Model = db.collection('users');
  });

  service.hooks(hooks);
};
