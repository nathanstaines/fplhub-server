const app = require('./app');
const isSameDay = require('date-fns/isSameDay');
const jobs = require('./utils/jobs');
const logger = require('./utils/logger');

const server = app.listen(app.get('port'));

process.on('unhandledRejection', (reason, p) =>
  logger.error('Unhandled Rejection at: Promise ', p, reason)
);

server.on('listening', () => {
  logger.info(
    'server listening on %s://%s:%d',
    app.get('protocol'),
    app.get('host'),
    app.get('port')
  );

  const mongoClient = app.get('mongoClient');

  mongoClient.then(() => {
    const params = {
      query: {
        title: 'bootstrapAll'
      }
    };

    function initialiseJobs() {
      app
        .service('api/jobs')
        .find()
        .then(res => {
          res.data.forEach(job => {
            if (job.isEnabled && jobs(job.title)) {
              jobs(job.title).start();
              logger.info(`start cron job: ${job.title}`);
            }
          });
        })
        .catch(err => logger.error(err));
    }

    app
      .service('api/jobs')
      .find(params)
      .then(res => {
        const {data, total} = res;

        if (
          total === 0 ||
          (!data[0].isEnabled && !isSameDay(new Date(), data[0].updatedAt))
        ) {
          app
            .service('api/jobs')
            .update(
              'bootstrapAll',
              {isEnabled: true},
              {mongodb: {upsert: true}}
            )
            .then(() => initialiseJobs());
        } else {
          initialiseJobs();
        }
      })
      .catch(err => logger.error(err));
  });
});
