const {ApiKeyStrategy} = require('./strategies/api-key');

const {
  AuthenticationService,
  JWTStrategy
} = require('@feathersjs/authentication');

const {LocalStrategy} = require('@feathersjs/authentication-local');

module.exports = app => {
  const auth = new AuthenticationService(app, 'auth');

  auth.register('jwt', new JWTStrategy());
  auth.register('local', new LocalStrategy());
  auth.register('apiKey', new ApiKeyStrategy());

  app.use('/auth', auth);
};
