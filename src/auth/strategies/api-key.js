const {AuthenticationBaseStrategy} = require('@feathersjs/authentication');
const {NotAuthenticated} = require('@feathersjs/errors');

module.exports.ApiKeyStrategy = class ApiKeyStrategy extends AuthenticationBaseStrategy {
  constructor(headerField = 'x-api-key') {
    super();

    this.headerField = headerField;
  }

  authenticate(authentication) {
    const {apiKey} = authentication;
    const allowedKeys = this.app.get('api').keys.split(',');
    const match = allowedKeys.includes(apiKey);

    if (!match) {
      throw new NotAuthenticated('Invalid API key');
    }

    return {
      apiKey: true
    };
  }

  parse(req) {
    const headerField = req.headers[this.headerField];

    if (headerField) {
      return {
        strategy: this.name,
        apiKey: headerField
      };
    }

    return null;
  }
};
