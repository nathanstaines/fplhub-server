const socketio = require('@feathersjs/socketio');

module.exports = socketio(
  {
    cookie: false,
    handlePreflightRequest: (req, res) => {
      const headers = {
        'Access-Control-Allow-Headers':
          'Content-Type, Authorization, x-api-key',
        'Access-Control-Allow-Origin': req.headers.origin,
        'Access-Control-Allow-Credentials': true
      };
      res.writeHead(200, headers);
      res.end();
    }
  },
  io => {
    io.use((socket, next) => {
      socket.feathers.headers = socket.handshake.headers;
      next();
    });
  }
);
