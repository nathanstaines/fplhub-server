const logger = require('./utils/logger');

module.exports = function(app) {
  // If no real-time functionality has been configured just return.
  if (typeof app.channel !== 'function') {
    return;
  }

  app.on('connection', connection => {
    app.channel('anonymous').join(connection);
  });

  app.publish('bootstrapped', () => {
    logger.info('Bootstrapped');

    return app.channel('anonymous');
  });
};
