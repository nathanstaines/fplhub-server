const {MongoClient} = require('mongodb');

module.exports = function(app) {
  const {name, uri} = app.get('database');

  const promise = MongoClient.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).then(client => client.db(name));

  app.set('mongoClient', promise);
};
