const auth = require('./auth');
const channels = require('./channels');
const compress = require('compression');
const configuration = require('@feathersjs/configuration');
const cors = require('cors');
const express = require('@feathersjs/express');
const favicon = require('serve-favicon');
const feathers = require('@feathersjs/feathers');
const helmet = require('helmet');
const logger = require('./utils/logger');
const mongodb = require('./mongodb');
const path = require('path');
const services = require('./services');
const socketio = require('./socketio');

const app = express(feathers());

app.configure(configuration());
app.use(cors({origin: app.get('cors').origin.split(',')}));
app.use(helmet());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
app.use('/', express.static(app.get('public')));
app.configure(express.rest());
app.configure(socketio);
app.configure(mongodb);
app.configure(auth);
app.configure(services);
app.configure(channels);
app.use(express.notFound());
app.use(express.errorHandler({html: false, logger}));

module.exports = app;
